//package com.example.recordersurfaceviewtest;
//
//import android.hardware.Camera;
//import android.media.CamcorderProfile;
//import android.media.MediaRecorder;
//import android.os.Bundle;
//import android.os.Environment;
//import android.util.Log;
//import android.view.Surface;
//import android.view.SurfaceHolder;
//import android.view.SurfaceView;
//import android.view.View;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.Toast;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.example.recordersurfaceviewtest.view.CameraHelper;
//
//import java.io.File;
//import java.util.List;
//import java.util.Random;
//import java.util.Timer;
//import java.util.TimerTask;
//
////https://www.jianshu.com/p/96ee1b7e67e3
//public class MainActivity_test extends AppCompatActivity implements SurfaceHolder.Callback{
//
//    private int mScreenWidth;
//    private int mScreenHeight;
//    private Camera mCamera;
//    private Camera.Parameters mParameters;
//    private List<int[]> mFpsRange;
//    private String mDirName;
//    private File mVecordFile;
//    private MediaRecorder mMediaRecorder;
//    private Timer mTimer;
//    private TimerTask timerTask;
//    private int mTimeCount;
//    private boolean isStarting;
//    private int mCameraPosition = 0;
//    private Camera.Size mOptimalSize;
//    private LinearLayout lay_tool;
//    private Button tag_start;
//    private ProgressBar mProgressBar;
//    private SurfaceView mSurfaceView;
//    private SurfaceHolder mSurfaceHolder;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        initView();
//    }
//    public void initView(){
//        lay_tool = findViewById(R.id.lay_tool);
//        tag_start = findViewById(R.id.tag_start);
//        mProgressBar = findViewById(R.id.mProgressBar);
//        mSurfaceView = findViewById(R.id.surfaceview);
//
//        mSurfaceHolder = mSurfaceView.getHolder();
//        mSurfaceHolder.addCallback(this);
//        mSurfaceHolder.setFixedSize(mScreenWidth, mScreenHeight);
//        mSurfaceHolder.setKeepScreenOn(true);
//
//    }
//    @Override
//    public void surfaceCreated(SurfaceHolder mSurfaceHolder) {
//        try {
//            if (mCamera == null) {
//                openCamera();
//            }
//            if (null != mCamera) {
//                mCamera.setPreviewDisplay(mSurfaceHolder);//Camera屏幕通过SurfaceHolder与SurfaceView 进行绑定
//                mCamera.startPreview();//开始预览
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Toast.makeText(MainActivity_test.this, "打开相机失败", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    @Override
//    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//
//        mScreenWidth = width;
//        mScreenHeight = height;
//        setCameraParameters();
//
//    }
//
//    @Override
//    public void surfaceDestroyed(SurfaceHolder holder) {
//        releaseCameraResource();
//    }
//
//    /**
//     * 打开相机
//     */
//    private void openCamera() {
//        if (null != mCamera) {
//            releaseCameraResource();
//        }
//        try {
//            if (!checkCameraFacing(0) && !checkCameraFacing(1)) {
//                Toast.makeText(MainActivity_test.this, "未发现有可用摄像头", Toast.LENGTH_SHORT).show();
//                return;
//            }
//            if (!checkCameraFacing(mCameraPosition)) {
//                Toast.makeText(MainActivity_test.this, mCameraPosition == 0 ? "后置摄像头不可用" : "前置摄像头不可用", Toast.LENGTH_SHORT).show();
//                return;
//            }
//            mCamera = Camera.open(mCameraPosition);
////            mCamera = Camera.open(0);
//        } catch (Exception e) {
//            e.printStackTrace();
//            releaseCameraResource();
//        }
//    }
//
//    /**
//     * 检查是否有摄像头
//     *
//     * @param facing 前置还是后置
//     * @return
//     */
//    private boolean checkCameraFacing(int facing) {
//        int cameraCount = Camera.getNumberOfCameras();
//        Camera.CameraInfo info = new Camera.CameraInfo();
//        for (int i = 0; i < cameraCount; i++) {
//            Camera.getCameraInfo(i, info);
//            if (facing == info.facing) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    //设置相机参数
//    private void setCameraParameters(){
//        try {
////            mCamera = Camera.open();// 打开摄像头
//            if (mCamera == null)
//                return;
////            mCamera.setDisplayOrientation(90);//将展示方向旋转90度
////            mCamera.setPreviewDisplay(mSurfaceHolder);//Surface 预览
//            //可以通过获取相机的参数实例，设置里面各种效果，包括刚刚的预览图，前置摄像头，闪光灯等
//            mParameters = mCamera.getParameters();// 获得相机参数
//
////            //设置图片格式
////            mParameters.setPictureFormat(ImageFormat.JPEG);
////            mParameters.setJpegQuality(100);
////            mParameters.setJpegThumbnailQuality(100);
//
////            mParameters.setPictureFormat(PixelFormat.JPEG);//设定图片格式为JPEG 默认为NV21
////            mParameters.setPreviewFormat(PixelFormat.YCbCr_420_SP);//设置预览版式为YCbCr_420_SP 默认为NV21
//
//            //该方法返回了SurfaceView的宽与高，根据给出的尺寸与宽高比例，获取一个最适配的预览尺寸
//            List<Camera.Size> mSupportedPreviewSizes = mParameters.getSupportedPreviewSizes();
//            List<Camera.Size> mSupportedVideoSizes = mParameters.getSupportedVideoSizes();
//            mOptimalSize = CameraHelper.getOptimalVideoSize(mSupportedVideoSizes,
//                    mSupportedPreviewSizes, mScreenWidth, mScreenHeight);
//            //该方法是获取最佳的预览与摄像尺寸。然后设置预览图像大小
//            mParameters.setPreviewSize(mOptimalSize.width, mOptimalSize.height); // 设置预览图像大小
//
//            mCamera.setDisplayOrientation(getDegree());
////            if(this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE){
////                //如果是竖屏
//////                mParameters.set("orientation", "portrait");
////                Log.e("lu","我是竖屏............");
////                //在2.2以上可以使用
////                mCamera.setDisplayOrientation(90);
////            }else{
//////                mParameters.set("orientation", "landscape");
////                Log.e("lu","我是横屏............");
////                //在2.2以上可以使用
////                mCamera.setDisplayOrientation(0);
////            }
//
//            List<String> focusModes = mParameters.getSupportedFocusModes();
//            if (focusModes.contains("continuous-video")) {
//                mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
//            }
//            mFpsRange =  mParameters.getSupportedPreviewFpsRange();
//
//            List<String> modes = mParameters.getSupportedFocusModes();
//            if (modes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
//                //支持自动聚焦模式
//                mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
//            }
//
//            mCamera.setParameters(mParameters);// 设置相机参数
////            mCamera.startPreview();// 开始预览
//
//
//            //假设要支持自动对焦功能，则在需要的情况下，或者在上述surfaceChanged调用完startPreview函数后，可以调用Camera::autoFocus函数来设置自动对焦回调函数，该步是可选操作，有些设备可能不支持，可以通过Camera::getFocusMode函数查询。代码可以参考如下：
//            // 自动对焦
////            mCamera.autoFocus(new Camera.AutoFocusCallback(){
////                @Override
////                public void onAutoFocus(boolean success, Camera camera){
////                    if (success){
////                        // success为true表示对焦成功，改变对焦状态图像
////                        ivFocus.setImageResource(R.drawable.focus2);
////                    }
////                }
////            });
//
//
//        }catch (Exception io){
//            io.printStackTrace();
//        }
//    }
//
//
//    private int getDegree() {
//        //获取当前屏幕旋转的角度
//        int rotating = this.getWindowManager().getDefaultDisplay().getRotation();
//        int degree = 0;//度数
//        //根据手机旋转的角度，来设置surfaceView的显示的角度
//        switch (rotating) {
//            case Surface.ROTATION_0:
//                degree = 90;
//                break;
//            case Surface.ROTATION_90:
//                degree = 0;
//                break;
//            case Surface.ROTATION_180:
//                degree = 270;
//                break;
//            case Surface.ROTATION_270:
//                degree = 180;
//                break;
//        }
//        return degree;
//    }
//    /**
//     * 释放摄像头资源
//     */
//    private void releaseCameraResource() {
//        if (mCamera != null) {
//            mCamera.setPreviewCallback(null);
//            mCamera.stopPreview();
//            mCamera.lock();
//            mCamera.release();
//            mCamera = null;
//        }
//    }
//    /**
//     * 这两个队列分别是 该相机支持的 预览大小（一般就是拍照时照片的大小），另外一个就是支持适配的大小，
//     * 因为都是队列，说明相机支持很多组尺寸，而且，照片的尺寸与视频的尺寸是不一样的。我debug看了几款手机，
//     * 通常摄像支持的尺寸少一点，照片会多一些。这样，我们就要通过刚刚方法给出的宽高，
//     * 获取一个最佳匹配的预览尺寸.
//     *
//     * @param supportedVideoSizes Supported camera video sizes.
//     * @param previewSizes Supported camera preview sizes.
//     * @param w     The width of the view.
//     * @param h     The height of the view.
//     * @return Best match camera video size to fit in the view.
//     */
//    public static Camera.Size getOptimalVideoSize(List<Camera.Size> supportedVideoSizes,
//                                                  List<Camera.Size> previewSizes, int w, int h) {
//        // Use a very small tolerance because we want an exact match.
//        final double ASPECT_TOLERANCE = 0.1;
//        double targetRatio = (double) w / h;
//
//        // Supported video sizes list might be null, it means that we are allowed to use the preview
//        // sizes
//        List<Camera.Size> videoSizes;
//        if (supportedVideoSizes != null) {
//            videoSizes = supportedVideoSizes;
//        } else {
//            videoSizes = previewSizes;
//        }
//        Camera.Size optimalSize = null;
//
//        // Start with max value and refine as we iterate over available video sizes. This is the
//        // minimum difference between view and camera height.
//        double minDiff = Double.MAX_VALUE;
//
//        // Target view height
//        int targetHeight = h;
//
//        // Try to find a video size that matches aspect ratio and the target view size.
//        // Iterate over all available sizes and pick the largest size that can fit in the view and
//        // still maintain the aspect ratio.
//        for (Camera.Size size : videoSizes) {
//            double ratio = (double) size.width / size.height;
//            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
//                continue;
//            if (Math.abs(size.height - targetHeight) < minDiff && previewSizes.contains(size)) {
//                optimalSize = size;
//                minDiff = Math.abs(size.height - targetHeight);
//            }
//        }
//
//        // Cannot find video size that matches the aspect ratio, ignore the requirement
//        if (optimalSize == null) {
//            minDiff = Double.MAX_VALUE;
//            for (Camera.Size size : videoSizes) {
//                if (Math.abs(size.height - targetHeight) < minDiff && previewSizes.contains(size)) {
//                    optimalSize = size;
//                    minDiff = Math.abs(size.height - targetHeight);
//                }
//            }
//        }
//        return optimalSize;
//    }
//
//    /**
//     * 创建目录与文件
//     */
//    private void createRecordDir() {
//        mDirName = String.valueOf(System.currentTimeMillis()) +  String.valueOf( new Random().nextInt(1000));
//        File FileDir = new File(Environment.getExternalStorageState() + mDirName);
//        Log.e("eeeeee","eeeeeee当前视频保存的路径是"+FileDir);
//        if (!FileDir.exists()) {
//            FileDir.mkdirs();
//        }
//        // 创建文件
//        try {
//            mVecordFile = new File(FileDir.getAbsolutePath() + "/"  +".mp4");
//            Log.e("Path:", mVecordFile.getAbsolutePath());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 录制前，初始化
//     */
//    private void initRecord() {
//        try {
//            //进入一个预览的拍摄页面了，该页面其实也可以用来做拍照。
//            // 要想做拍摄，还要实例化MediaRecorder，然后传入camera并初始化相应的参数。
//            if(mMediaRecorder == null){
//                mMediaRecorder = new MediaRecorder();
//            }
//            if(mCamera != null){
//                mCamera.unlock();
//                mMediaRecorder.setCamera(mCamera);
//            }
//
////            mMediaRecorder.setOnErrorListener(this);
//
////            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);//音频源  麦克风
////            mMediaRecorder.setAudioChannels(1);//单声道
////            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);//音频格式
//
//            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);//音频源  麦克风
//            // 设置录制视频源为Camera(相机)
//            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);//视频源
////            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);//视频输出格式
////            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);//视频录制格式
//            mMediaRecorder.setOrientationHint(90);//视频旋转90度
//
//            // Use the same size for recording profile.
//            CamcorderProfile mProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
//            mProfile.videoFrameWidth = mOptimalSize.width;
//            mProfile.videoFrameHeight = mOptimalSize.height;
////
//            mMediaRecorder.setProfile(mProfile);
//            //该设置是为了抽取视频的某些帧，真正录视频的时候，不要设置该参数
////            mMediaRecorder.setCaptureRate(mFpsRange.get(0)[0]);//获取最小的每一秒录制的帧数
//
//
////            // 设置视频录制的分辨率。必须放在设置编码和格式的后面，否则报错,而且这个值要适配
////            //手机，不然也会在后面stop方法报错！
////            mMediaRecorder.setVideoSize(1280,720);
////            // 设置录制的视频帧率。必须放在设置编码和格式的后面，否则报错，这样设置变清晰
////            mMediaRecorder.setVideoEncodingBitRate(10*1024*1024);
//
//            mMediaRecorder.setOutputFile(mVecordFile.getAbsolutePath());
//
//            mMediaRecorder.prepare();
//            mMediaRecorder.start();
//        } catch (Exception e) {
//            e.printStackTrace();
//            releaseRecord();
//        }
//    }
//
//
//    /**
//     * 开始录制视频
//     */
//    public void startRecord(View view) {
////        this.mOnRecordFinishListener = onRecordFinishListener;
//        isStarting = true;
//        lay_tool.setVisibility(View.INVISIBLE);
//        tag_start.setVisibility(View.VISIBLE);
////        anim.start();
//        createRecordDir();
//        try {
//            initRecord();
//            mTimeCount = 0;// 时间计数器重新赋值
//            mTimer = new Timer();
//            timerTask = new TimerTask() {
//                @Override
//                public void run() {
//                    mTimeCount++;
//                    mProgressBar.setProgress(mTimeCount);
////                    if (mTimeCount == mRecordMaxTime) {// 达到指定时间，停止拍摄
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                stop();
////                                if (mOnRecordFinishListener != null){
////                                    mOnRecordFinishListener.onRecordFinish();
////                                }
//                            }
//                        });
////                    }
//                }
//            };
//            mTimer.schedule(timerTask, 0, 100);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 停止拍摄
//     */
//    public void stop() {
//        stopRecord();
//        releaseRecord();
//        releaseCameraResource();
//    }
//
//    /**
//     * 停止录制
//     */
//    public void stopRecord() {
//        mProgressBar.setProgress(0);
//        isStarting = false;
//        tag_start.setVisibility(View.GONE);
////        anim.stop();
//        lay_tool.setVisibility(View.VISIBLE);
//        if(timerTask != null)
//            timerTask.cancel();
//        if (mTimer != null)
//            mTimer.cancel();
//        if (mMediaRecorder != null) {
//            try {
//                mMediaRecorder.stop();
//                mMediaRecorder.reset();
//            } catch (IllegalStateException e) {
//                e.printStackTrace();
//            } catch (RuntimeException e) {
//                e.printStackTrace();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 释放资源
//     */
//    private void releaseRecord() {
//        if (mMediaRecorder != null) {
//            mMediaRecorder.setPreviewDisplay(null);
//            mMediaRecorder.setOnErrorListener(null);
//            try {
//                mMediaRecorder.release();
//            } catch (IllegalStateException e) {
//                e.printStackTrace();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        mMediaRecorder = null;
//    }
//
//    //闪光灯关闭与开启
////    private void flashLightToggle(){
////        try {
////            if(isFlashLightOn){
////                mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);//关闭闪光灯
////                mCamera.setParameters(mParameters);
////                isFlashLightOn = false;
////            }else {
////                mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);//开启闪光灯
////                mCamera.setParameters(mParameters);
////                isFlashLightOn = true;
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
//    //前后摄像头切换，就要重新初始化 camera实例
////    private void switchCamera(){
////        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
////        int cameraCount = Camera.getNumberOfCameras();//得到摄像头的个数
////
////        for(int i = 0; i < cameraCount; i++ ) {
////            Camera.getCameraInfo(i, cameraInfo);//得到每一个摄像头的信息
////            if(mCameraPosition == 1) {
////                //现在是后置，变更为前置
////                if(cameraInfo.facing  == Camera.CameraInfo.CAMERA_FACING_FRONT) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
////                    mCamera.stopPreview();//停掉原来摄像头的预览
////                    mCamera.release();//释放资源
////                    mCamera = null;//取消原来摄像头
////                    mCamera = Camera.open(i);//打开当前选中的摄像头
////                    try {
////                        mCamera.setDisplayOrientation(90);// 打开摄像头并将展示方向旋转90度
////                        mCamera.setPreviewDisplay(mSurfaceHolder);//通过surfaceview显示取景画面
////                    } catch (IOException e) {
////                        // TODO Auto-generated catch block
////                        e.printStackTrace();
////                    }
////                    mCamera.setParameters(mParameters);// 设置相机参数
////                    mCamera.startPreview();//开始预览
////                    mCameraPosition = 0;
////                    break;
////                }
////            } else {
////                //现在是前置， 变更为后置
////                if(cameraInfo.facing  == Camera.CameraInfo.CAMERA_FACING_BACK) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
////                    mCamera.stopPreview();//停掉原来摄像头的预览
////                    mCamera.release();//释放资源
////                    mCamera = null;//取消原来摄像头
////                    mCamera = Camera.open(i);//打开当前选中的摄像头
////                    try {
////                        mCamera.setDisplayOrientation(90);// 打开摄像头并将展示方向旋转90度
////                        mCamera.setPreviewDisplay(mSurfaceHolder);//通过surfaceview显示取景画面
////                    } catch (IOException e) {
////                        // TODO Auto-generated catch block
////                        e.printStackTrace();
////                    }
////                    mCamera.setParameters(mParameters);// 设置相机参数
////                    mCamera.startPreview();//开始预览
////                    mCameraPosition = 1;
////                    break;
////                }
////            }
////
////        }
////    }
//}