package com.example.recordersurfaceviewtest.util;

import android.util.Log;

public class LogUtils {
    public static boolean isShowLog = true;
    public static String tag = "eeeeeee";
    public static void d(String msg){
        if (isShowLog){
           Log.d(tag,msg);
        }
    }
    public static void e(String msg){
        if (isShowLog){
            Log.e(tag,msg);
        }
    }
}
